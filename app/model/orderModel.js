//Import thư viện mongoose
const mongoose = require("mongoose");

//Class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo orderSchema từ class schema
const orderSchema = new Schema ({
    orderCode: {
        type: String, 
        unique: true
    },
	pizzaSize: {
        type: String, 
        required: true
    },
	pizzaType: {
        type: String, 
        required: true
    },
	voucher: {
        type: mongoose.ObjectId,
        ref: "Voucher"
    },
    drink: {
        type: mongoose.ObjectId, 
        ref: "Drink"
    },
	status:{
        type: String, 
        required: true
    }
});
// Biên dịch order Model từ orderSchema
module.exports = mongoose.model("Order", orderSchema);