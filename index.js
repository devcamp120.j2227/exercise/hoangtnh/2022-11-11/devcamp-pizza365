//import express js
const express = require("express");
//import thư viện path
const path = require ("path");

// Khai báo thư viện Mongoose
const mongoose = require("mongoose");

//khởi tạo app
const app = express();

//khai báo cổng chạy app
const port = 8000;

//khai báo model
const drinkModel = require("./app/model/drinkModel");
const voucherModel = require("./app/model/voucherModel");
const orderModel = require("./app/model/orderModel");
const userModel = require("./app/model/userModel");

//call api chạy project pizza 365
app.get("/",(request, response) =>{
    response.sendFile(path.join(__dirname + "/views/Task 43.40.html"))
})
//hiển thị hình ảnh cần thêm  middleware static vào express
app.use(express.static(__dirname +"/views"));

//kết nối với mongodb
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza", (error) => {
    if(error) throw error;
    console.log("Connect pizza365 to MongoDB successfully!");
})

//chạy app trên cổng 
app.listen(port, ()=>{
    console.log(`App is running on port ${port}`)
})
